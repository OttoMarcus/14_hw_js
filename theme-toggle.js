
const wrapBackground = document.querySelector("html")
const btn = document.getElementsByClassName("button-toggle");
const wrapper = document.getElementsByClassName("wrapper");
const currentTheme = localStorage.getItem('theme');
const icon = document.getElementsByClassName("button-toggle span");

document.body.addEventListener('click', (event) => {
    event.preventDefault();
    if (event.target.closest(".button-toggle") && localStorage.getItem("theme") === "dark") {
        localStorage.removeItem("theme");

    }
    else {
        localStorage.setItem("theme", "dark");
    }
    addDarkTheme();
});
    function addDarkTheme() {
        if (localStorage.getItem("theme") === "dark") {
            wrapBackground.classList.add("dark-theme");
        }
        else {
            wrapBackground.classList.remove("dark-theme")
        }
    }
addDarkTheme();
